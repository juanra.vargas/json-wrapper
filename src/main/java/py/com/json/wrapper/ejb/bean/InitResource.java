/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.json.wrapper.ejb.bean;

import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Juan Vargas <juan.vargas@konecta.com.py at Konecta S.A.>
 */
@Path("/test")
public class InitResource {

    @Inject
    private InitSingleton sing;

    @GET
    @Path("/llamada")
    @Produces(MediaType.APPLICATION_JSON)
    public Map realizarLlamada() {
        sing.realizarLlamada();
        Map m = new HashMap();
        m.put("status", 0);
        m.put("message", "OK");
        return m;
    }

    @GET
    @Path("/actualizar")
    @Produces(MediaType.APPLICATION_JSON)
    public Map actualizar() {
        sing.establecerUsuariosActivos();
        Map m = new HashMap();
        m.put("status", 0);
        m.put("message", "OK");
        return m;
    }

    @GET
    @Path("/fuera-linea")
    @Produces(MediaType.APPLICATION_JSON)
    public Map solucionarFueraDeLinea() {
        sing.solucionarFueraDeLinea();
        Map m = new HashMap();
        m.put("status", 0);
        m.put("message", "OK");
        return m;
    }

    @GET
    @Path("/sin-retorno")
    @Produces(MediaType.APPLICATION_JSON)
    public Map solucionarSinRetorno() {
        sing.solucionarSinRetorno();
        Map m = new HashMap();
        m.put("status", 0);
        m.put("message", "OK");
        return m;
    }

    @GET
    @Path("/todos")
    @Produces(MediaType.APPLICATION_JSON)
    public Map solucionarTodos() {
        sing.solucionarTodosMenosSinRetronoAndFueraDeLinea();
        Map m = new HashMap();
        m.put("status", 0);
        m.put("message", "OK");
        return m;
    }
}
