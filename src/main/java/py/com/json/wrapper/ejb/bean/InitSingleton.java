/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.json.wrapper.ejb.bean;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import py.com.json.wrapper.ejb.Mapa;

/**
 *
 * @author Juan Vargas <juan.vargas@konecta.com.py at Konecta S.A.>
 */
@Singleton
@Startup
public class InitSingleton {

    @EJB
    private LlamadaBean llamadaBean;
    @EJB
    private WorkerBean workBean;
    @EJB
    private OtroBean otroBean;

    @PostConstruct
    public void init() {
        System.out.println("-----------------------------INICIANDO SINGLETON---------------------");
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Schedule(second = "10", minute = "*/5", hour = "*", persistent = false)
    public void realizarLlamada() {
        llamadaBean.realizarLlamada();
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Schedule(second = "0", minute = "*/1", hour = "*", persistent = false)
    public void establecerUsuariosActivos() {
        System.out.println("-----------------ACTUALIZANDO USUARIOS-----------------");
        try {
            List<Mapa> usuarios = workBean.getUsuariosActivos();
            List<Mapa> incidencias = workBean.getIncidenciasAActualizar();
            otroBean.establecerUsuariosActivos(incidencias, usuarios);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Schedule(second = "0", minute = "*/1", hour = "*", persistent = false)
    public void solucionarFueraDeLinea() {
        System.out.println("-----------------ACTUALIZANDO FUERA DE LINEA-----------------");
        try {
            List<Mapa> list = workBean.getRegistrosToUpdate(43L, "30");
            otroBean.solucionarFueraDeLinea(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Schedule(second = "0", minute = "*/1", hour = "*", persistent = false)
    public void solucionarSinRetorno() {
        System.out.println("-----------------ACTUALIZANDO SIN RETORNO-----------------");
        try {
            List<Mapa> list = workBean.getRegistrosToUpdate(61L, "60");
            otroBean.solucionarSinRetorno(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    @Schedule(second = "0", minute = "*/1", hour = "*", persistent = false)
    public void solucionarTodosMenosSinRetronoAndFueraDeLinea() {
        System.out.println("-----------------ACTUALIZANDO TODOS-----------------");
        try {
            List<Mapa> list = workBean.getRegistrosToUpdatePerHour();
            otroBean.solucionarTodosMenosSinRetornoAndFueraDeLinea(list);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
