/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.json.wrapper.ejb.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import oracle.jdbc.OracleTypes;
import py.com.json.wrapper.ejb.ConnectionManager;
import py.com.json.wrapper.ejb.Constantes;
import py.com.json.wrapper.ejb.Mapa;
import py.com.json.wrapper.ejb.model.CrmWeser;

/**
 *
 * @author Juan Vargas <juan.vargas@konecta.com.py at Konecta S.A.>
 */
@Stateless
public class AuxAsyncBean {

    @Asynchronous
    public Future<Integer> insertarIncidenciaAsync(Long vehiculoId, Long clienteId, Long alertaId, Timestamp fecha, String imei, Long idUsuario, String urlDireccion,
            String simNumber, String deviceModel,
            String iconId, String additionalNotes) {
        return new AsyncResult<>(insertarIncidencia(vehiculoId, clienteId, alertaId, fecha, imei, idUsuario, urlDireccion, simNumber, deviceModel, iconId, additionalNotes));
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Integer insertarIncidencia(Long vehiculoId, Long clienteId, Long alertaId, Timestamp fecha, String imei, Long idUsuario, String urlDireccion,
            String simNumber, String deviceModel,
            String iconId, String additionalNotes) {
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            c.setAutoCommit(false);
            PreparedStatement ps = c.prepareStatement("INSERT INTO skn.crm_insi (insi_codi, insi_vehi_codi, insi_clien_codi, insi_alert_codi, insi_esta, "
                    + "insi_fech, INSI_CANT, insi_user_codi, insi_url, insi_imei, INSI_SIM, INSI_ICON, INSI_NOTA, INSI_DEMO  ) "
                    + "  VALUES ( ?, ?, ?, ?, 1, ?, 1, ?, ?, ?, ?, ?, ?, ?) ");
            Long nextValIncidencia = getMaxIncidencia();
            ps.setLong(1, nextValIncidencia);
            if (vehiculoId == null) {
                ps.setNull(2, OracleTypes.NULL);
            } else {
                ps.setLong(2, vehiculoId);
            }
            ps.setLong(3, clienteId);
            ps.setLong(4, alertaId);
            ps.setTimestamp(5, fecha);
            ps.setLong(6, idUsuario);
            ps.setString(7, urlDireccion);
            ps.setString(8, imei);
            ps.setString(9, simNumber);
            ps.setString(10, iconId);
            ps.setString(11, additionalNotes);
            ps.setString(12, deviceModel);
            ps.executeUpdate();
            c.commit();

            System.out.println("DATOS EN crm_insi INSERTADOS, ID INCIDENCIA: " + nextValIncidencia.intValue());
            PreparedStatement ps2 = c.prepareStatement(" INSERT INTO skn.crm_vehi_emei (veem_vehi_codi, veem_nume_emei, veem_fecha) "
                    + "  VALUES (?, ?, ?) ");
            if (vehiculoId == null) {
                ps2.setNull(1, OracleTypes.NULL);
            } else {
                ps2.setLong(1, vehiculoId);
            }
            ps2.setString(2, imei);
            ps2.setTimestamp(3, fecha);
            ps2.executeUpdate();
            c.commit();
            System.out.println("DATOS EN crm_vehi_emei INSERTADOS");
            PreparedStatement ps3 = c.prepareStatement(" INSERT INTO skn.crm_insi_deta (inde_codi, inde_insi_codi, inde_esde_codi, inde_user_codi, inde_come, inde_fecha) "
                    + "  VALUES (?, ?, ?, ?, ?, ?) ");
            Long nextInsiDeta = getNextInsiDeta();
            ps3.setLong(1, nextInsiDeta);
            ps3.setLong(2, nextValIncidencia);
            ps3.setLong(3, 1L);
            ps3.setLong(4, 1L);
            ps3.setString(5, "Recepción de incidencia");
            ps3.setTimestamp(6, fecha);
            ps3.executeUpdate();
            c.commit();
            System.out.println("DATOS EN crm_insi_deta INSERTADOS, ID DETALLE: " + nextInsiDeta.intValue());
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException ex) {
                Logger.getLogger(WorkerAsyncBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            ConnectionManager.closeConnection(c);
        }

        return 1;
    }

    private long getNextInsiDeta() {
        Long maxInsiDeta = null;
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("select sec_crm_esta_deta.nextval as valor from dual");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                maxInsiDeta = rs.getLong("valor");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return maxInsiDeta;
    }

    private Long getMaxIncidencia() {
        Long maxIncidencia = null;
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("select sec_crm_insi.nextval as valor from dual");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                maxIncidencia = rs.getLong("valor");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return maxIncidencia;
    }

    @Asynchronous
    public Future<Integer> insertarCrmWeserAsync(CrmWeser pojo) {
        return new AsyncResult<>(insertarCrmWeser(pojo));
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Integer insertarCrmWeser(CrmWeser pojo) {
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            c.setAutoCommit(false);
            PreparedStatement ps = c.prepareStatement("INSERT INTO crm_weser (weser_codi, weser_clie, weser_chas, WESER_ALER_ID, weser_hora, weser_emei) "
                    + "VALUES (?, ?, ?, ?, ?, ?) ");

            Long nextVal = getCrmWeserNextVal();
            ps.setLong(1, nextVal);
            ps.setString(2, pojo.getWeserClie());
            ps.setString(3, pojo.getWeserChas());
            ps.setLong(4, this.getAlertaId(pojo.getWeserAler()));
            ps.setTimestamp(5, new java.sql.Timestamp(pojo.getWeserHora().getTime()));
            ps.setString(6, pojo.getWeserEmei());
            ps.executeUpdate();
            c.commit();
        } catch (Exception e) {
            e.printStackTrace();
            try {
                c.rollback();
            } catch (SQLException ex) {
                Logger.getLogger(WorkerAsyncBean.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return 1;
    }

    private Long getCrmWeserNextVal() {
        Connection c = null;
        Long nextVal = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("select seccrm_weser.nextval as valor from dual");
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                nextVal = rs.getLong("valor");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return nextVal;
    }

    @Asynchronous
    public Future<Integer> actualizarIncidenciaAsync(CrmWeser pojo) {
        return new AsyncResult<>(actualizarIncidencia(pojo));
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Integer actualizarIncidencia(CrmWeser pojo) {
        System.out.println("CUENTA NUEVA INCIDENCIA: " + pojo);
        // deberiamos actualizar todos los registros que cumplan la condicion
        // en crm insi
        Map ownerChapa = this.getOwnerChapa(pojo.getWeserChas());
        Long alertaId = this.getAlertaId(pojo.getWeserAler());
        if (alertaId != null) {
            Long vehiculoId;
            Long clienteId;
            if (pojo.getWeserChas().equals("123456789")) {
                clienteId = 10062038201L;
                vehiculoId = null;
            } else {
                clienteId = Double.valueOf(ownerChapa.get("cliente_id").toString()).longValue();
                vehiculoId = Double.valueOf(ownerChapa.get("vehiculo_id").toString()).longValue();
            }

            List<Mapa> list = this.getValoresAActualizar(clienteId, vehiculoId, alertaId);
            System.out.println("cantidad de registros a actualizar: " + list.size());
            this.incrementarIncidencia(list, pojo);
        } else {
            System.out.println("NO EXISTIO LA ALERTA " + pojo.getWeserAler());
        }
        return 1;
    }

    public Long getAlertaId(String alerta) {
        Connection c = null;
        Long idAlerta = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("select "
                    + "	a.alert_codi as alerta_id "
                    + "from "
                    + "	CRM_ALERT a "
                    + "where "
                    + "	trim(upper(a.alert_desc)) like trim(upper(?))");
            alerta = alerta.concat("%");
            String valor;
            if (alerta.contains("DISPOSITIVO")) {
                valor = "DISPOSITIVO".concat("%");
            } else if (alerta.contains("DURACION")) {
                valor = "DURACION".concat("%");
            } else {
                valor = alerta;
            }

            ps.setString(1, valor);
            ResultSet rs = ps.executeQuery();
            for (; rs.next();) {
                idAlerta = rs.getLong("alerta_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return idAlerta;
    }

    private Map getOwnerChapa(String nroChasis) {
        Connection c = null;
        Map resp = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("select "
                    + "	cv.vehi_clpr_codi as cliente_id, "
                    + "	cv.vehi_codi as vehiculo_id "
                    + "from "
                    + "	COME_VEHI cv "
                    + "where "
                    + "	trim(cv.vehi_nume_chas) = trim(?)");

            ps.setString(1, nroChasis);
            ResultSet rs = ps.executeQuery();
            resp = new HashMap();
            if (rs.next()) {
                resp.put("cliente_id", rs.getLong("cliente_id"));
                resp.put("vehiculo_id", rs.getLong("vehiculo_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return resp;
    }

    private List<Mapa> getValoresAActualizar(Long idCliente, Long idVehiculo, Long idAlerta) {
        Connection c = null;
        List<Mapa> list = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            String sql = "select "
                    + "	* "
                    + "from "
                    + "	skn.crm_insi ci "
                    + "where "
                    + "	ci.insi_esta != 3 "
                    + "	and ci.insi_clien_codi = #{idCliente} "
                    + "	and (ci.insi_vehi_codi = #{idVehiculo} "
                    + "	or cast(#{idVehiculo} as number) is null) "
                    + "	and ci.insi_alert_codi = #{idAlerta} ";
            sql = sql.replaceAll("\\#\\{idCliente\\}", String.valueOf(idCliente))
                    .replaceAll("\\#\\{idVehiculo\\}", String.valueOf(idVehiculo))
                    .replaceAll("\\#\\{idAlerta\\}", String.valueOf(idAlerta))
                    .replaceAll("\\#\\{maxVal\\}", "60");

            PreparedStatement ps = c.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();
            list = new LinkedList<>();
            for (; rs.next();) {
                Mapa m = new Mapa();
                for (int i = 1; i <= columns; i++) {
                    m.put(md.getColumnName(i), rs.getObject(i));
                }
                list.add(m);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return list;
    }

    private void incrementarIncidencia(List<Mapa> list, CrmWeser pojo) {
        Connection c = null;
        try {
            System.out.println("[DEBE CONTAR]");
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            c.setAutoCommit(false);
            for (Mapa mapa : list) {
                Long idIncidencia = Double.valueOf(mapa.get("insiCodi").toString()).longValue();
                String sql = "select "
                        + "	INSI_CODI,INSI_VEHI_CODI,INSI_CLIEN_CODI,INSI_USER_CODI,INSI_ALERT_CODI,INSI_ESTA,INSI_FECH,INSI_CANT "
                        + "from "
                        + "	skn.crm_insi ci "
                        + "where "
                        + "	ci.insi_codi = #{idIncidencia} "
                        + " for update ";
                sql = sql.replaceAll("\\#\\{idIncidencia\\}", String.valueOf(idIncidencia));
                Statement stmt = c.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                ResultSet rs = stmt.executeQuery(sql);
                for (; rs.next();) {
                    Long cantidad = rs.getLong("insi_cant");
                    rs.updateLong("insi_cant", cantidad += 1);
                    rs.updateTimestamp("insi_fech", new java.sql.Timestamp(pojo.getWeserHora().getTime()));
                    rs.updateRow();
                }
                c.commit();
            }
        } catch (Exception e) {
            try {
                if (c != null && !c.isClosed()) {
                    c.rollback();
                }
            } catch (SQLException ex) {
                Logger.getLogger(WorkerBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
    }

    @Asynchronous
    public Future<Integer> actualizarWeserAsync(List<Map> list) {
        return new AsyncResult<>(actualizarWeser(list));
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    private Integer actualizarWeser(List<Map> list) {
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            for (Map map : list) {

                Long weserCodi = Double.valueOf(map.get("weserCodi").toString()).longValue();
                Date fecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(map.get("weserHora").toString());

                String sql = "update crm_weser set weser_hora = ? where weser_codi = ?";
                PreparedStatement ps = c.prepareStatement(sql);
                ps.setTimestamp(1, new java.sql.Timestamp(fecha.getTime()));
                ps.setLong(2, weserCodi);
                ps.executeUpdate();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return 1;
    }

    public Long contarValores(CrmWeser obj, Long idCliente, Long idVehiculo, Long idAlerta) {
        Long resp = null;
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }

            String sql = "select "
                    + "	count(1) as valor "
                    + "from "
                    + "	crm_insi "
                    + "where "
                    + "	insi_clien_codi = ? "
                    + "	and insi_vehi_codi = ? "
                    + "	and insi_alert_codi = ? "
                    + "	and insi_imei = ? "
                    + "	and insi_esta != 3 ";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setLong(1, idCliente);
            if (idVehiculo == null) {
                ps.setNull(2, OracleTypes.NULL);
            } else {
                ps.setLong(2, idVehiculo);
            }
            ps.setLong(3, idAlerta);
            ps.setString(4, obj.getWeserEmei());
            ResultSet rs = ps.executeQuery();
            for (; rs.next();) {
                resp = rs.getLong("valor");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return resp;
    }

}
