/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.json.wrapper.ejb.bean;

import py.com.json.wrapper.ejb.RestCaller;
import com.google.gson.Gson;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import py.com.json.wrapper.ejb.Constantes;

/**
 *
 * @author Juan Vargas <juan.vargas@konecta.com.py at Konecta S.A.>
 */
@Stateless
public class LlamadaBean {

    @Inject
    private OtroBean otroBean;
    @Inject
    private RestCaller restCaller;

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void realizarLlamada() {
        int i = 0;
        Boolean deboContinuar = true;
        try {
            while (deboContinuar) {
                String urlFinal = Constantes.URL_SERVICE
                        .replaceAll("\\#\\{page\\}", String.valueOf(++i))
                        .replaceAll("\\#\\{fecha\\}", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                System.out.println("SE LLAMA A LA API A LAS: " + new SimpleDateFormat("HH:mm:ss").format(new Date()));
                String response = restCaller.getCall(urlFinal, Integer.MAX_VALUE, Integer.MAX_VALUE);
                Gson g = new Gson();
                Map responseM = g.fromJson(response, Map.class);
                Map items = (Map) responseM.get("items");
                List<Map> data = (List<Map>) items.get("data");
                otroBean.resolverLista(data);
                if (((Map) responseM.get("items")).get("next_page_url") == null) {
                    deboContinuar = false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
