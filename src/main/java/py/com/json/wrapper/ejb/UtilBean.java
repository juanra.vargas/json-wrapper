/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.json.wrapper.ejb;

import java.text.Normalizer;
import java.util.HashSet;
import java.util.Map;

/**
 *
 * @author Juan Vargas <juan.vargas@konecta.com.py at Konecta S.A.>
 */
public class UtilBean {

    public static void normalizar(HashSet<Map> list) {
        for (Map map : list) {
            for (Object object : map.keySet()) {
                String element = map.get(object).toString();
                element = Normalizer.normalize(element, Normalizer.Form.NFKD).replaceAll("[^\\p{ASCII}]", "").toUpperCase().trim();
                map.put(object, element);
            }
        }
    }
}
