/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.json.wrapper.ejb;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juan Vargas <juan.vargas@konecta.com.py at Konecta S.A>
 */
public class ConnectionManager {

    public static Connection getConnection() {
        String driverName = "org.postgresql.Driver";

        String url = "jdbc:postgresql://localhost:5432/async";
        String uname = "postgres";
        String passwd = "postgres";

        Connection con = null;
        try {
            Class.forName(driverName);
            con = DriverManager.getConnection(url, uname, passwd);
            con.setAutoCommit(false);
        } catch (SQLException | ClassNotFoundException e) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, e);
        }
        return con;
    }

    public static Connection getConnection(String driverName, String url, String userName, String password) {
        if (driverName == null) {
            driverName = "org.postgresql.Driver";
        }
        if (url == null) {
            url = "jdbc:postgresql://localhost:5432/async";
        }
        if (userName == null) {
            userName = "postgres";
        }
        if (password == null) {
            password = "postgres";
        }

        Connection con = null;
        try {
            Class.forName(driverName);
            con = DriverManager.getConnection(url, userName, password);
        } catch (SQLException | ClassNotFoundException e) {
            Logger.getLogger(Connection.class.getName()).log(Level.SEVERE, null, e);
        }
        return con;
    }

    public static void closeConnection(Connection c) {
        try {
            if (c != null && !c.isClosed()) {
                c.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void closeCallableStatement(CallableStatement cs) {
        try {
            if (cs != null && !cs.isClosed()) {
                cs.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
