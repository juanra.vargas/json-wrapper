/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.json.wrapper.ejb.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import py.com.json.wrapper.ejb.ConnectionManager;
import py.com.json.wrapper.ejb.Constantes;
import py.com.json.wrapper.ejb.Mapa;
import py.com.json.wrapper.ejb.model.CrmWeser;

/**
 *
 * @author Juan Vargas <juan.vargas@konecta.com.py at Konecta S.A.>
 */
@Stateless
public class WorkerAsyncBean {

    @EJB
    private AuxAsyncBean bean;

    public Integer procesar(CrmWeser pojo, Long idUsuario, String latitud, String longitud) {
        System.out.println("CrmWeser=" + pojo);
        System.out.println("idUsuario=" + idUsuario);
        System.out.println("latitud=" + latitud + "longitud=" + longitud);
        Boolean noExiste;
        Connection c = null;

        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("select "
                    + "	count(1) as valor "
                    + " from "
                    + "	crm_weser cw "
                    + " where "
                    + " trim(cw.weser_chas) = ?"
                    + "	and trim(cw.weser_emei) = ?"
                    + " and WESER_ALER_ID = ?"
                    + " and to_char(cw.weser_hora, 'DD/MM/YYYY') = ?");
            ps.setString(1, pojo.getWeserChas());
            ps.setString(2, pojo.getWeserEmei());
            ps.setLong(3, bean.getAlertaId(pojo.getWeserAler()));
            ps.setString(4, new SimpleDateFormat("dd/MM/yyyy").format(pojo.getWeserHora()));
            ResultSet rs = ps.executeQuery();
            Long valor = null;
            for (; rs.next();) {
                valor = rs.getLong("valor");
            }
            noExiste = valor == 0L;

            Long clienteId;
            Long vehiculoId;
            Map ownerChapa = this.getOwnerChapa(pojo.getWeserChas());
            if (pojo.getWeserChas().equals("123456789")) {
                clienteId = 10062038201L;
                vehiculoId = null;
            } else {
                clienteId = Double.valueOf(ownerChapa.get("cliente_id").toString()).longValue();
                vehiculoId = Double.valueOf(ownerChapa.get("vehiculo_id").toString()).longValue();
            }
            Long alertaId = this.getAlertaId(pojo.getWeserAler());
            if (noExiste) {
                System.out.println("NO EXISTE REGISTRO ----->  INSERTA UNA NUEVA INCIDENCIA: " + pojo);
                bean.insertarCrmWeserAsync(pojo).get();

                if (alertaId != null) {
                    String urlDireccion = "";
                    if (latitud != null && longitud != null) {
                        urlDireccion = "https://www.google.com/maps/search/?api=1&query=".concat(latitud).concat(",").concat(longitud);
                    }
                    Double iconId = Double.valueOf(pojo.getWeserIconId());
                    iconId += 1D;
                    bean.insertarIncidenciaAsync(
                            vehiculoId,
                            clienteId,
                            alertaId,
                            new java.sql.Timestamp(pojo.getWeserHora().getTime()),
                            pojo.getWeserEmei(),
                            idUsuario,
                            urlDireccion,
                            pojo.getWeserSim(),
                            pojo.getWeserDeviceModel(),
                            String.valueOf(iconId.intValue()),
                            pojo.getWeserAdditionalNotes()
                    ).get();
                } else {
                    System.out.println("NO EXISTIO LA ALERTA " + pojo.getWeserAler());
                }

            } else {
                ps = c.prepareStatement("select "
                        + "     cw.weser_codi as id,"
                        + "	cw.weser_hora as hora "
                        + " from "
                        + "	crm_weser cw "
                        + " where "
                        + "	trim(cw.weser_chas) = ?"
                        + "	and trim(cw.weser_emei) = ?"
                        + "     and WESER_ALER_ID = ? "
                        + "     order by cw.weser_hora desc");
                ps.setString(1, pojo.getWeserChas().trim());
                ps.setString(2, pojo.getWeserEmei().trim());
                ps.setLong(3, bean.getAlertaId(pojo.getWeserAler()));
                rs = ps.executeQuery();
                boolean deboActualizar = false;
                boolean mismaHora = false;
                List<Map> weserUpdate = new LinkedList<>();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                for (; rs.next();) {
                    Timestamp ts = rs.getTimestamp("hora");
                    Date dateBd = new Date(ts.getTime());
                    System.out.println("hora bd: " + sdf.format(dateBd));
                    System.out.println("hora api: " + sdf.format(new Date(pojo.getWeserHora().getTime())));

                    if (dateBd.before(pojo.getWeserHora())) {
                        Map m = new HashMap();
                        m.put("weserHora", sdf.format(pojo.getWeserHora()));
                        m.put("weserCodi", rs.getLong("id"));
                        weserUpdate.add(m);
                    }
                    Long diff = pojo.getWeserHora().getTime() - ts.getTime();
                    Long diffMin = diff / (1000L * 60L);
                    System.out.println("diffMin: " + diffMin);
                    if (diffMin > 0L && diffMin <= 60L) {
                        // SE REPITE LA INCIDENCIA, SE DEBE ACTUALIZAR
                        System.out.println("SE REPITE LA INCIDENCIA, SE DEBE ACTUALIZAR");
                        deboActualizar = true;
                        break;
                    } else if (diffMin > 60L) {
                        System.out.println("DIFERENCIA MAYOR A 60");
                        deboActualizar = false;
                    } else if (diffMin == 0L) {
                        mismaHora = true;
                        deboActualizar = false;
                        break;
                    } else if (diffMin < 0) {
                        mismaHora = true;
                        deboActualizar = false;
                        break;
                    }

                }
                Long cantReg = bean.contarValores(pojo, clienteId, vehiculoId, alertaId);
                if (cantReg > 0) {
                    System.out.println("YA EXISTE EL REGISTRO EN CRM_INSI");
                    bean.actualizarIncidenciaAsync(pojo).get();
                } else {
                    bean.actualizarWeserAsync(weserUpdate).get();
                    System.out.println("debe actualizar? " + deboActualizar);
                    System.out.println("coincide la hora? " + mismaHora);
                    if (!mismaHora) {
                        this.insertarNuevaIncidencia(pojo, latitud, longitud, idUsuario);
                    }
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return 1;
    }

    private void insertarNuevaIncidencia(CrmWeser pojo, String latitud, String longitud, Long idUsuario) {
        try {
            System.out.println("INSERTA NUEVA INCIDENCIA POR DIFERENCIA MAYOR A 60: " + pojo);
            // aqui se debe insertar de nuevo la incidencia 
            Map ownerChapa = this.getOwnerChapa(pojo.getWeserChas());
            Long alertaId = this.getAlertaId(pojo.getWeserAler());

            if (alertaId != null) {
                Long vehiculoId;
                Long clienteId;
                if (pojo.getWeserChas().equals("123456789")) {
                    clienteId = 10062038201L;
                    vehiculoId = null;
                } else {
                    clienteId = Double.valueOf(ownerChapa.get("cliente_id").toString()).longValue();
                    vehiculoId = Double.valueOf(ownerChapa.get("vehiculo_id").toString()).longValue();
                }
                String urlDireccion = "";
                if (latitud != null && longitud != null) {
                    urlDireccion = "https://www.google.com/maps/search/?api=1&query=".concat(latitud).concat(",").concat(longitud);
                }
                Double iconId = Double.valueOf(pojo.getWeserIconId());
                iconId += 1D;
                Long cantReg = bean.contarValores(pojo, clienteId, vehiculoId, alertaId);
                if (cantReg != null && cantReg == 0) {
                    bean.insertarIncidenciaAsync(
                            vehiculoId,
                            clienteId,
                            alertaId,
                            new java.sql.Timestamp(pojo.getWeserHora().getTime()),
                            pojo.getWeserEmei(),
                            idUsuario,
                            urlDireccion,
                            pojo.getWeserSim(),
                            pojo.getWeserDeviceModel(),
                            String.valueOf(iconId.intValue()),
                            pojo.getWeserAdditionalNotes()
                    ).get();
                } else {
                    System.out.println("HORA REPETIDA " + pojo.getWeserAler());
                }

            } else {
                System.out.println("NO EXISTIO LA ALERTA " + pojo.getWeserAler());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Long getAlertaId(String alerta) {
        Connection c = null;
        Long idAlerta = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("select "
                    + "	a.alert_codi as alerta_id "
                    + "from "
                    + "	CRM_ALERT a "
                    + "where "
                    + "	trim(upper(a.alert_desc)) like trim(upper(?))");
            alerta = alerta.concat("%");
            String valor;
            if (alerta.contains("Dispositivo")) {
                valor = "Dispositivo".concat("%");
            } else if (alerta.contains("Duración")) {
                valor = "Duración".concat("%");
            } else {
                valor = alerta;
            }

            ps.setString(1, valor);
            ResultSet rs = ps.executeQuery();
            for (; rs.next();) {
                idAlerta = rs.getLong("alerta_id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return idAlerta;
    }

    private Map getOwnerChapa(String nroChasis) {
        Connection c = null;
        Map resp = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("select "
                    + "	cv.vehi_clpr_codi as cliente_id, "
                    + "	cv.vehi_codi as vehiculo_id "
                    + "from "
                    + "	COME_VEHI cv "
                    + "where "
                    + "	trim(cv.vehi_nume_chas) = trim(?)");

            ps.setString(1, nroChasis);
            ResultSet rs = ps.executeQuery();
            resp = new HashMap();
            if (rs.next()) {
                resp.put("cliente_id", rs.getLong("cliente_id"));
                resp.put("vehiculo_id", rs.getLong("vehiculo_id"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return resp;
    }

    private List<Mapa> getValoresAActualizar(Long idCliente, Long idVehiculo, Long idAlerta) {
        Connection c = null;
        List<Mapa> list = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            String sql = "select "
                    + "	* "
                    + "from "
                    + "	skn.crm_insi ci "
                    + "where "
                    + "	ci.insi_esta != 3 "
                    + "	and ci.insi_clien_codi = #{idCliente} "
                    + "	and (ci.insi_vehi_codi = #{idVehiculo} "
                    + "	or cast(#{idVehiculo} as number) is null) "
                    + "	and ci.insi_alert_codi = #{idAlerta} ";
            sql = sql.replaceAll("\\#\\{idCliente\\}", String.valueOf(idCliente))
                    .replaceAll("\\#\\{idVehiculo\\}", String.valueOf(idVehiculo))
                    .replaceAll("\\#\\{idAlerta\\}", String.valueOf(idAlerta))
                    .replaceAll("\\#\\{maxVal\\}", "60");

            PreparedStatement ps = c.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();
            list = new LinkedList<>();
            for (; rs.next();) {
                Mapa m = new Mapa();
                for (int i = 1; i <= columns; i++) {
                    m.put(md.getColumnName(i), rs.getObject(i));
                }
                list.add(m);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return list;
    }

    private void incrementarIncidencia(List<Mapa> list) {
        Connection c = null;
        try {
            System.out.println("[DEBE CONTAR]");
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            c.setAutoCommit(false);
            for (Mapa mapa : list) {
                Long idIncidencia = Double.valueOf(mapa.get("insiCodi").toString()).longValue();
                String sql = "select "
                        + "	INSI_CODI,INSI_VEHI_CODI,INSI_CLIEN_CODI,INSI_USER_CODI,INSI_ALERT_CODI,INSI_ESTA,INSI_FECH,INSI_CANT "
                        + "from "
                        + "	skn.crm_insi ci "
                        + "where "
                        + "	ci.insi_codi = #{idIncidencia} "
                        + " for update ";
                sql = sql.replaceAll("\\#\\{idIncidencia\\}", String.valueOf(idIncidencia));
                Statement stmt = c.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                ResultSet rs = stmt.executeQuery(sql);
                for (; rs.next();) {
                    Long cantidad = rs.getLong("insi_cant");
                    rs.updateLong("insi_cant", cantidad += 1);
                    rs.updateRow();
                }
                c.commit();
            }
        } catch (Exception e) {
            try {
                if (c != null && !c.isClosed()) {
                    c.rollback();
                }
            } catch (SQLException ex) {
                Logger.getLogger(WorkerBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
    }

}
