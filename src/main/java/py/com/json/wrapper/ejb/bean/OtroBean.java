/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.json.wrapper.ejb.bean;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import py.com.json.wrapper.ejb.Mapa;
import py.com.json.wrapper.ejb.UtilBean;
import py.com.json.wrapper.ejb.model.CrmWeser;

/**
 *
 * @author Juan Vargas <juan.vargas@konecta.com.py at Konecta S.A.>
 */
@Stateless
public class OtroBean {

    @Inject
    private WorkerBean workBean;

//    @Asynchronous
//    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void resolverLista(List<Map> list) {
        System.out.println("IN: cantidad de elementos=" + list.size());
        long t1 = System.currentTimeMillis();
        try {
            List<Mapa> usuarios = workBean.getUsuariosActivos();

            for (Map map : list) {
                String valorAlert;
                if (map.get("message").toString().contains("Dispositivo")) {
                    valorAlert = "Dispositivo Desconectado";
                } else if (map.get("message").toString().contains("Duración")) {
                    valorAlert = "Duración fuera de línea más larga que (60 minutos)";
                } else {
                    valorAlert = map.get("message").toString();
                }
                map.put("message", valorAlert);
            }
            HashSet<Map> s = new HashSet<>();
            for (Map map : list) {
                Map m = new HashMap();
                m.put("message", map.get("message"));
                m.put("vin", ((Map) map.get("device")).get("vin") == null || ((Map) map.get("device")).get("vin").toString().isEmpty() ? "123456789" : ((Map) map.get("device")).get("vin").toString());
                m.put("device_name", map.get("device_name").toString());
                m.put("imei", ((Map) map.get("device")).get("imei").toString());
                Date fecha = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(map.get("time").toString());
                m.put("time", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(fecha));
                m.put("additional_notes", ((Map) map.get("device")).get("additional_notes"));
                m.put("device_model", ((Map) map.get("device")).get("device_model"));
                m.put("icon_id", ((Map) map.get("device")).get("icon_id"));
                m.put("sim_number", ((Map) map.get("device")).get("sim_number"));
                m.put("latitude", map.get("latitude"));
                m.put("longitude", map.get("longitude"));
                s.add(m);
            }
            UtilBean.normalizar(s);

            for (Map map : s) {
                CrmWeser p = new CrmWeser();
                p.setWeserAler(map.get("message").toString().trim());
                p.setWeserChas(map.get("vin").toString().trim());
                p.setWeserClie(map.get("device_name").toString().trim());
                p.setWeserEmei(map.get("imei").toString().trim());
                p.setWeserHora(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(map.get("time").toString().trim()));
                p.setWeserAdditionalNotes(map.get("additional_notes").toString().trim());
                p.setWeserDeviceModel(map.get("device_model").toString().trim());
                p.setWeserIconId(map.get("icon_id").toString().trim());
                p.setWeserSim(map.get("sim_number").toString().trim());
                Long idUsuario;
                if (usuarios.isEmpty()) {
                    idUsuario = 1L;
                } else {
                    Random r = new Random();
                    Integer indexUsuario = r.nextInt(usuarios.size());
                    Mapa m = usuarios.get(indexUsuario);
                    idUsuario = Double.valueOf(m.get("tuseUserCodi").toString()).longValue();
                }
                String latitud = null, longitud = null;
                if (map.containsKey("latitude") && map.containsKey("longitude")) {
                    latitud = map.get("latitude").toString();
                    longitud = map.get("longitude").toString();
                }
                workBean.insertarPojo(p, idUsuario, latitud, longitud);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            long t2 = System.currentTimeMillis();
            System.out.println("SE INSERTARON APROX 30 REGISTROS EN : " + (t2 - t1) + " MILLISEGUNDOS");
        }
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void establecerUsuariosActivos(List<Mapa> incidencias, List<Mapa> usuarios) {
        try {
            Random r = new Random();
            for (Mapa incidencia : incidencias) {
                Long usuarioSaliente = Double.valueOf(incidencia.get("insiUserCodi").toString()).longValue();
                Long usuarioEntrante;
                if (usuarios.isEmpty()) {
                    usuarioEntrante = 1L;
                } else {
                    usuarioEntrante = Double.valueOf(usuarios.get(r.nextInt(usuarios.size())).get("tuseUserCodi").toString()).longValue();
                }
                Long idIncidencia = Double.valueOf(incidencia.get("insiCodi").toString()).longValue();
                workBean.actualizarIncidencia(usuarioEntrante, idIncidencia);
                workBean.insertarHistorial(idIncidencia, usuarioEntrante, usuarioSaliente, new Date(new Date().getTime()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void solucionarSinRetorno(List<Mapa> list) {
        try {
            for (Mapa mapa : list) {
                Long idIncidencia = Double.valueOf(mapa.get("insiCodi").toString()).longValue();
                workBean.updateRegistros(idIncidencia);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void solucionarFueraDeLinea(List<Mapa> list) {
        try {
            for (Mapa mapa : list) {
                Long idIncidencia = Double.valueOf(mapa.get("insiCodi").toString()).longValue();
                workBean.updateRegistros(idIncidencia);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void solucionarTodosMenosSinRetornoAndFueraDeLinea(List<Mapa> list) {
        try {
            for (Mapa mapa : list) {
                Long idIncidencia = Double.valueOf(mapa.get("insiCodi").toString()).longValue();
                workBean.updateRegistros(idIncidencia);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
