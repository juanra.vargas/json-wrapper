/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.json.wrapper.ejb;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.ejb.Stateless;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 *
 * @author Juan Vargas <juan.vargas@konecta.com.py at Konecta S.A>
 */
@Stateless
public class RestCaller {

    public static Client CLIENTE;

    public String getCall(String url, Integer connTime, Integer readTime) {
        try {
            System.out.println("IN: url=" + url);
            Client client = getCliente();
            client.setConnectTimeout(connTime);
            client.setReadTimeout(readTime);
            WebResource webResource = client.resource(url);

            ClientResponse getResponse
                    = webResource
                            .type("application/json;charset=utf-8")
                            .get(ClientResponse.class);
            String response = getResponse.getEntity(String.class);

            if (getResponse.getStatus() == 200) {
                return response;
            } else {
                System.out.println("status: " + getResponse.getStatus());
                System.out.println("JSON OUT: [{}]" + response.replaceAll("\n", ", "));
                return null;
            }
        } catch (ClientHandlerException | UniformInterfaceException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String postCall(String url, Integer connTime, Integer readTime, Object request) {
        try {
            System.out.println("IN: [url=" + url + " request=" + request);
            Client client = getCliente();
            client.setConnectTimeout(connTime);
            client.setReadTimeout(readTime);
            WebResource webResource = client.resource(url);

            Gson g = new Gson();
            String json = g.toJson(request);
            System.out.println("JSON IN: [{}]" + json);
            ClientResponse postResponse
                    = webResource
                            .type("application/json;charset=utf-8")
                            .post(ClientResponse.class, json);
            String response = postResponse.getEntity(String.class);

            if (postResponse.getStatus() == 200) {
                return response;
            } else {
                System.out.println("status: " + postResponse.getStatus());
                System.out.println("JSON OUT: " + response.replaceAll("\n", ", "));
                return null;
            }
        } catch (ClientHandlerException | UniformInterfaceException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Client getCliente() {
        if (CLIENTE == null) {
            try {

                TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return new X509Certificate[0];
                    }

                    @Override
                    public void checkServerTrusted(X509Certificate[] arg0, String arg1)
                            throws CertificateException {

                    }

                    @Override
                    public void checkClientTrusted(X509Certificate[] arg0, String arg1)
                            throws CertificateException {

                    }
                }};

                // Install the all-trusting trust manager
                HostnameVerifier hostnameVerifier = new HostnameVerifier() {
                    @Override
                    public boolean verify(String string, SSLSession ssls) {
                        return true;
                    }
                };
                ClientConfig config = new DefaultClientConfig();
                SSLContext ctx = SSLContext.getInstance("SSL");
                ctx.init(null, trustAllCerts, null);
                config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hostnameVerifier, ctx));
                CLIENTE = Client.create(config);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        return CLIENTE;
    }
}
