package py.com.json.wrapper.ejb.model;

import java.io.Serializable;
import java.util.Date;

public class CrmWeser implements Serializable {

    private Long weserCodi;
    private String weserClie;
    private String weserChas;
    private String weserAler;
    private Date weserHora;
    private String weserEmei;
    private String weserSim;
    private String weserDeviceModel;
    private String weserIconId;
    private String weserAdditionalNotes;

    public CrmWeser() {
    }

    public Long getWeserCodi() {
        return weserCodi;
    }

    public void setWeserCodi(Long weserCodi) {
        this.weserCodi = weserCodi;
    }

    public String getWeserClie() {
        return weserClie;
    }

    public void setWeserClie(String weserClie) {
        this.weserClie = weserClie;
    }

    public String getWeserChas() {
        return weserChas;
    }

    public void setWeserChas(String weserChas) {
        this.weserChas = weserChas;
    }

    public String getWeserAler() {
        return weserAler;
    }

    public void setWeserAler(String weserAler) {
        this.weserAler = weserAler;
    }

    public Date getWeserHora() {
        return weserHora;
    }

    public void setWeserHora(Date weserHora) {
        this.weserHora = weserHora;
    }

    public String getWeserEmei() {
        return weserEmei;
    }

    public void setWeserEmei(String weserEmei) {
        this.weserEmei = weserEmei;
    }

    public String getWeserSim() {
        return weserSim;
    }

    public void setWeserSim(String weserSim) {
        this.weserSim = weserSim;
    }

    public String getWeserDeviceModel() {
        return weserDeviceModel;
    }

    public void setWeserDeviceModel(String weserDeviceModel) {
        this.weserDeviceModel = weserDeviceModel;
    }

    public String getWeserIconId() {
        return weserIconId;
    }

    public void setWeserIconId(String weserIconId) {
        this.weserIconId = weserIconId;
    }

    public String getWeserAdditionalNotes() {
        return weserAdditionalNotes;
    }

    public void setWeserAdditionalNotes(String weserAdditionalNotes) {
        this.weserAdditionalNotes = weserAdditionalNotes;
    }

    @Override
    public String toString() {
        return "CrmWeser{" + "weserCodi=" + weserCodi + ", weserClie=" + weserClie + ", weserChas=" + weserChas + ", weserAler=" + weserAler
                + ", weserHora=" + weserHora + ", weserEmei=" + weserEmei + ", weserSim=" + weserSim + ", weserDeviceModel=" + weserDeviceModel
                + ", weserIconId=" + weserIconId + ", weserAdditionalNotes=" + weserAdditionalNotes + '}';
    }

}
