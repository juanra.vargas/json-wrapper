/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.json.wrapper.ejb.bean;

import py.com.json.wrapper.ejb.ConnectionManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Future;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import py.com.json.wrapper.ejb.model.CrmWeser;
import py.com.json.wrapper.ejb.Constantes;
import py.com.json.wrapper.ejb.Mapa;

/**
 *
 * @author Juan Vargas <juan.vargas@konecta.com.py at Konecta S.A.>
 */
@Stateless
public class WorkerBean {

    @EJB
    private WorkerAsyncBean asyncBean;

    private void procesar(CrmWeser pojo, Long idUsuario, String latitud, String longitud) {
        try {
            asyncBean.procesar(pojo, idUsuario, latitud, longitud);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void insertarPojo(CrmWeser pojo, Long idUsuario, String latitud, String longitud) {
        try {
            procesar(pojo, idUsuario, latitud, longitud);
        } catch (Exception e) {
            System.out.println("OCURRIO UNA EXCEPCION DE SQL AL INTENTAR INSERTAR: " + e.getMessage());
        }
    }

    public Future<Integer> desactivarUsuariosAsync() {
        return new AsyncResult<>(desactivarUsuarios());
    }

    public Integer desactivarUsuarios() {
        Integer rows = 0;
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("update skn.CRM_TURN_USER ctu set ctu.TUSE_ESTA = 'I' where ctu.TUSE_ESTA = 'A'");
            rows = ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return rows;
    }

    public Long getCodigoTurno(String horaTurno) {
        Long idTurno = null;
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("select turn_codi as codigo_entrada from skn.crm_turn where TURN_ENTR = ?");
            ps.setString(1, horaTurno);
            ResultSet rs = ps.executeQuery();
            for (; rs.next();) {
                idTurno = rs.getLong("codigo_entrada");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }

        return idTurno;
    }

    public List<Mapa> getUsuariosActivos() {
        System.out.println("IN:");
        List<Mapa> list = null;
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("select TUSE_USER_CODI from skn.CRM_TURN_USER where TUSE_ESTA = 'A' and TUSE_CODI <> 21");
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();
            list = new LinkedList<>();
            System.out.println("[Se obtienen los usuarios]");
            for (; rs.next();) {
                Mapa m = new Mapa();
                for (int i = 1; i <= columns; i++) {
                    m.put(md.getColumnName(i), rs.getObject(i));
                }
                list.add(m);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }

        return list;
    }

    public List<Mapa> getIncidenciasAActualizar() {
        System.out.println("IN:");
        List<Mapa> list = null;
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("select * from skn.CRM_INSI where INSI_USER_CODI in (select TUSE_USER_CODI from skn.CRM_TURN_USER where TUSE_ESTA = 'I' or TUSE_CODI = 21) and INSI_ESTA <> 3");
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();
            list = new LinkedList<>();
            System.out.println("[Se obtienen las incidencias]");
            for (; rs.next();) {
                Mapa m = new Mapa();
                for (int i = 1; i <= columns; i++) {
                    m.put(md.getColumnName(i), rs.getObject(i));
                }
                list.add(m);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }

        return list;
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void setearUsuarioActivo(Long idUsuario) {
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("update skn.crm_turn_user set tuse_esta = 'A' where tuse_codi = ?");
            ps.setLong(1, idUsuario);
            ps.executeUpdate();
            System.out.println("USUARIO CON ID: [" + idUsuario + "] ACTUALIZADO A ACTIVO");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void insertarHistorial(Long idIncidencia, Long idNuevoUsuario, Long idUsuarioSaliente, Date fechaHoraCambio) {
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("insert into skn.crm_hist_insi(HIIN_INSI_CODI, HIIN_USER_CODI_PRIM, HIIN_USEG_CODI_SEGU, HIIN_FECHA_HORA) "
                    + "values(?, ?, ?, ?)");
            ps.setLong(1, idIncidencia);
            ps.setLong(2, idNuevoUsuario);
            ps.setLong(3, idUsuarioSaliente);
            ps.setTimestamp(4, new java.sql.Timestamp(fechaHoraCambio.getTime()));
            ps.executeUpdate();
            System.out.println("SE CAMBIO LA INCIDENCIA [" + idIncidencia + "] DE USUARIO: [" + idUsuarioSaliente + "] A USUARIO [" + idNuevoUsuario + "]");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
    }

    public List<Mapa> getIncidenciasPendientes() {
        List<Mapa> list = null;
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("select * from skn.crm_insi where insi_esta = ? ");
            ps.setLong(1, 1);
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();
            list = new LinkedList<>();
            for (; rs.next();) {
                Mapa m = new Mapa();
                for (int i = 1; i <= columns; i++) {
                    m.put(md.getColumnName(i), rs.getObject(i));
                }
                list.add(m);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return list;
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void actualizarIncidencia(Long idUsuario, Long idIncidencia) {
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("update skn.crm_insi set insi_user_codi = ? where insi_codi = ?");
            ps.setLong(1, idUsuario);
            ps.setLong(2, idIncidencia);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
    }

    public List<Mapa> getRegistrosToUpdate(Long idEstado, String intervalo) {
        Connection c = null;
        List<Mapa> list = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            String sql = "select "
                    + "	ci.insi_codi "
                    + "from "
                    + "	skn.crm_insi_conta cic "
                    + "join skn.crm_insi ci on "
                    + "	ci.insi_codi = cic.inco_insi_codi "
                    + "where "
                    + "	ci.insi_esta = #{idEstado} "
                    + "	and cast (cic.inco_prox_cont as timestamp) - cast (to_date('#{fecha}', 'DD/MM/YYYY HH24:MI:SS') as timestamp) >= interval '#{intervalo}' minute "
                    + "group by "
                    + "	ci.insi_codi ";
            sql = sql.replaceAll("\\#\\{idEstado\\}", String.valueOf(idEstado))
                    .replaceAll("\\#\\{intervalo\\}", intervalo)
                    .replaceAll("\\#\\{fecha\\}", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date(new Date().getTime())));

            PreparedStatement ps = c.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();
            list = new LinkedList<>();
            for (; rs.next();) {
                Mapa m = new Mapa();
                for (int i = 1; i <= columns; i++) {
                    m.put(md.getColumnName(i), rs.getObject(i));
                }
                System.out.println("agregando elemento a lista: " + m);
                list.add(m);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return list;
    }

    public List<Mapa> getRegistrosToUpdatePerHour() {
        Connection c = null;
        List<Mapa> list = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            String sql = "select "
                    + "	ci.insi_codi "
                    + "from "
                    + "	skn.crm_insi ci "
                    + "join skn.crm_insi_conta cic on "
                    + "	ci.insi_codi = cic.inco_insi_codi "
                    + "where "
                    + "	ci.insi_esta <> 61 and ci.insi_esta <> 43 "
                    + "and "
                    + "	to_char(cic.inco_prox_cont, 'DD/MM/YYYY') = ? "
                    + " group by ci.insi_codi ";

            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
            ResultSet rs = ps.executeQuery();
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();
            list = new LinkedList<>();
            for (; rs.next();) {
                Mapa m = new Mapa();
                for (int i = 1; i <= columns; i++) {
                    m.put(md.getColumnName(i), rs.getObject(i));
                }
                list.add(m);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
        return list;
    }

    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void updateRegistros(Long idIncidencia) {
        Connection c = null;
        try {
            if (c == null || c.isClosed()) {
                c = ConnectionManager.getConnection(Constantes.DRIVER_NAME, Constantes.URL_JDBC, Constantes.USER_NAME, Constantes.PASSWORD);
            }
            PreparedStatement ps = c.prepareStatement("UPDATE  skn.CRM_INSI SET INSI_ESTA = 1 WHERE INSI_CODI = ?");
            ps.setLong(1, idIncidencia);
            ps.executeUpdate();
            System.out.println("[Se actualizo la incidencia con id [" + idIncidencia + "]");
            PreparedStatement ps2 = c.prepareStatement("INSERT INTO skn.CRM_INSI_DETA(INDE_INSI_CODI, INDE_ESDE_CODI, INDE_USER_CODI,INDE_FECHA) VALUES (?, 1, 1,?)");
            ps2.setLong(1, idIncidencia);
            ps2.setTimestamp(2, new java.sql.Timestamp(new Date(new Date().getTime()).getTime()));
            ps2.executeUpdate();
            System.out.println("[Se inserto una nueva incidencia al usuario 1]");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            ConnectionManager.closeConnection(c);
        }
    }

}
